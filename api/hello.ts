import { NowRequest, NowResponse } from "@vercel/node";
import { buffer } from "micro";

function intToHex(integer) {
  let number = (+integer).toString(16).toUpperCase()
  if( (number.length % 2) > 0 ) { number= "0" + number }
  return number
}

export default async (request: NowRequest, response: NowResponse) => {

  const buf = await buffer(request);
  var arr = Array.prototype.slice.call(buf, 0);

  //const manufacturer = arr[2] + arr[3];
  const serial =
    `${intToHex(String(arr[7]))}` +
    `${intToHex(String(arr[6]))}` +
    `${intToHex(String(arr[5]))}` +
    `${intToHex(String(arr[4]))}`;


  console.log("delka ", arr[0],"vyrobni cislo", serial, " hodnota", arr[19], " cas", new Date().toLocaleString());
  response.status(200).send(`Hello!`);
};
