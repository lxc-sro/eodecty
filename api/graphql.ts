import { ApolloServer } from "@saeris/apollo-server-vercel";
import { GraphQLScalarType, Kind } from "graphql";
import fetch from "node-fetch";
import sha1 from "sha1";

// Construct a schema, using GraphQL schema language
const typeDefs = `
  scalar Date

  type Query {
    hello: String
    dataByToken(token: String, email: String, password: String): RootObject
  }

  type Mutation {
    setUserCredentials(token: String!, email: String!, password: String!): String
  }

  type DruhMerice {
    ID: Int
    Nazev: String
    Jednotka: String
  }

  type NamereneHodnoty {
    ID: Int
    DatumOdectu: Date
    DatumOd: Date
    DatumDo: Date
    PocatecniStav: Float
    KonecnyStav: Float
    Odecet: Float
  }

  type Merice {
    ID: Int
    PlatnostOd: Date
    PlatnostDo: Date
    DatumOvereni: Date
    VyrobniCislo: String
    Popis: String
    DruhMerice: DruhMerice
    NamereneHodnoty: [NamereneHodnoty]
  }

  type Sml {
    PlatnostOd: Date
    PlatnostDo: Date
    VariabilniSymbol: Int
    ID: Int
    Merice: [Merice]
  }

  type Osoby {
    Nazev: String
    Ulice: String
    UliceSCisly: String
    Misto: String
    sml: [Sml]
    Permission: String
    Token: String
  }

  type Jednotky {
    Oznaceni: String
    Popis: String
    ID: Int
    Osoby: [Osoby]
  }

  type CurrentStats {
    Nazev: String
    total: Float
  }

  type Domy {
    Oznaceni: String
    Popis: String
    ID: Int
    CurrentStats: [CurrentStats]
    Jednotky: [Jednotky]
  }

  type RootObject {
    Permission: Int
    Token: String
    LoginEmail: String
    ID: Int
    Nazev: String
    Ulice: String
    UliceSCisly: String
    Misto: String
    Domy: [Domy]
  }
`;

// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    hello: () => "Hello world!",
    async dataByToken(
      parent: any,
      args: { token: string; email: string; password: string },
      context: any,
      info: any
    ) {
      var token = args.token ? args.token : "";
      var email = args.email ? args.email : "";
      var password = args.password ? sha1(args.password) : "";
      const res = await fetch(
        "http://api.lxc.cz:8080/api/organizace/@Token='" +
          token +
          "',@LoginEmail='" +
          email +
          "',@LoginPassword='" +
          password +
          "'",
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((response) => response.json())
        .then((data) => data)
        .catch((error) => console.log("error", error));

      return res;
    },
  },
  Mutation: {
    async setUserCredentials(
      parent: any,
      args: {
        token: string;
        email: string;
        password: string;
      },
      context: any,
      info: any
    ) {
      const res = await fetch(
        "http://api.lxc.cz:8080/api/put/login/@Token='" +
          args.token +
          "',@LoginEmail='" +
          args.email +
          "',@LoginPassword='" +
          sha1(args.password) +
          "'",
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((response) => response.statusText)
        .catch((error) => console.log("error", error));

      return res;
    },
  },
  Date: new GraphQLScalarType({
    name: "Date",
    description: "Date custom scalar type",
    parseValue(value) {
      return new Date(value); // value from the client
    },
    serialize(value) {
      return new Date(value); // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10); // ast value is always in string format
      }
      return null;
    },
  }),
};

const server = new ApolloServer({
  typeDefs,
  resolvers,

  // By default, the GraphQL Playground interface and GraphQL introspection
  // is disabled in "production" (i.e. when `process.env.NODE_ENV` is `production`).
  //
  // If you'd like to have GraphQL Playground and introspection enabled in production,
  // the `playground` and `introspection` options must be set explicitly to `true`.
  playground: true,
  introspection: true,
});

export default server.createHandler();
