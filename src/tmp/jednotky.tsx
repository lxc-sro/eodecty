const Jednotky = [
  { id: 1, dum: 100, cislo: "1/A", vlastnik: 3 },
  { id: 2, dum: 100, cislo: "1/B", vlastnik: 4 },
  { id: 3, dum: 100, cislo: "2/A", vlastnik: 5 },
  { id: 4, dum: 100, cislo: "2/B", vlastnik: 6 },
  { id: 5, dum: 100, cislo: "3/A", vlastnik: 7 },
  { id: 6, dum: 100, cislo: "3/B", vlastnik: 8 },
  { id: 7, dum: 100, cislo: "3/C", vlastnik: 9 },
  { id: 8, dum: 101, cislo: "101/A", vlastnik: 11 },
  { id: 9, dum: 101, cislo: "101/B", vlastnik: 12 },
  { id: 10, dum: 101, cislo: "101/C", vlastnik: 13 },
  { id: 11, dum: 101, cislo: "101/D", vlastnik: 14 },
  { id: 12, dum: 101, cislo: "101/E", vlastnik: 15 },
];

export default Jednotky;
