

const Organizations = [
    {id: 1, name: "Profitherm", token:"profitherm", permission: 3},
    {id: 2, name: "Attica s.r.o.", token:"123456", permission: 2},
    {id: 3, name: "Jan Novák", token:"12345", permission: 1},
    {id: 4, name: "Jiří Mrak", token:"1234", permission: 0},
    {id: 5, name: "Libor Hurka", token:"123123", permission: 0},
    {id: 6, name: "Lenka Sedláčková", token:"12", permission: 0},
    {id: 7, name: "Karel Dvořák", token:"1", permission: 0},
    {id: 8, name: "Tomáš Svoboda", token:"abc", permission: 0},
    {id: 9, name: "Jiří Lhotka", token:"abcd", permission: 0},
    {id: 10, name: "Kerola s.r.o.", token:"demospravce", permission: 2},
    {id: 11, name: "Jitka Svojíková", token:"demopredseda", permission: 1},
    {id: 12, name: "Lukáš Prchal", token:"demo123", permission: 0},
    {id: 13, name: "David Fousek", token:"demo1234", permission: 0},
    {id: 14, name: "Anna Horáková", token:"demo12345", permission: 0},
    {id: 15, name: "Matěj Sedláček", token:"demovlastnik", permission: 0},
]

export default Organizations