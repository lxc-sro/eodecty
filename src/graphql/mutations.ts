import gql from "graphql-tag";

export const SET_LOGIN_CREDENTIALS = gql`
  mutation setLoginCredentials($token: String!, $email: String!, $password: String!) {
    setUserCredentials(token: $token, email: $email, password: $password)
  }
`;
