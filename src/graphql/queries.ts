import gql from "graphql-tag";

export const HELLO_QUERY = gql`
  query hello {
    hello
  }
`;

export const LOGIN = gql`
  query login($token: String, $email: String, $password: String) {
    dataByToken(token: $token, email: $email, password: $password) {
      ID
      Permission
      Token
      Nazev
      LoginEmail
    }
  }
`;

export const ODECTY_QUERY = gql`
  query odecty($token: String, $email: String, $password: String) {
    dataByToken(token: $token, email: $email, password: $password) {
      ID
      Permission
      Token
      LoginEmail
      Nazev
      Ulice
      UliceSCisly
      Misto
      Domy {
        ID
        Oznaceni
        Popis
        CurrentStats {
          Nazev
          total
        }
        Jednotky {
          ID
          Oznaceni
          Popis
          Osoby {
            Nazev
            Ulice
            UliceSCisly
            Misto
            Token
            Permission
            sml {
              ID
              PlatnostOd
              PlatnostDo
              VariabilniSymbol
              Merice {
                ID
                PlatnostOd
                PlatnostDo
                DatumOvereni
                Popis
                VyrobniCislo
                DruhMerice {
                  ID
                  Nazev
                  Jednotka
                }
                NamereneHodnoty {
                  ID
                  DatumOdectu
                  DatumOd
                  DatumDo
                  PocatecniStav
                  KonecnyStav
                  Odecet
                }
              }
            }
          }
        }
      }
    }
  }
`;
