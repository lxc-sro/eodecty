
    export interface DruhMerice {
        ID: number;
        Nazev: string;
        Jednotka: string;
    }

    export interface NamereneHodnoty {
        ID: number;
        DatumOdectu: Date;
        DatumOd: Date;
        DatumDo: Date;
        PocatecniStav: number;
        KonecnyStav: number;
        Odecet: number;
    }

    export interface Merice {
        ID: number;
        PlatnostOd: Date;
        PlatnostDo: Date;
        DatumOvereni: Date;
        VyrobniCislo: string;
        Popis: string;
        DruhMerice: DruhMerice[];
        NamereneHodnoty: NamereneHodnoty[];
    }

    export interface Sml {
        PlatnostOd: Date;
        PlatnostDo: Date;
        VariabilniSymbol: number;
        Merice: Merice[];
    }

    export interface Osoby {
        Nazev: string;
        Ulice: string;
        UliceSCisly: string;
        Misto: string;
        sml: Sml[];
        Permission: string;
        Token: string;
    }

    export interface Jednotky {
        Oznaceni: string;
        Popis: string;
        ID: number;
        Osoby: Osoby[];
    }

    export interface Domy {
        Oznaceni: string;
        Popis: string;
        ID: number;
        Jednotky: Jednotky[];
    }

    export interface RootObject {
        Nazev: string;
        Ulice: string;
        UliceSCisly: string;
        Misto: string;
        Domy: Domy[];
    }
