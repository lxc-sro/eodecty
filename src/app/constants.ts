import cookies from "js-cookie";
import jednotky from "../tmp/jednotky";
import merice from "../tmp/merice";
import domy from "../tmp/domy";
import organizations from "../tmp/organizations";

const UserCookie = cookies.get("user");

export const User = UserCookie && JSON.parse(UserCookie);


export const Jednotka =
  User && jednotky.find((jednotka) => jednotka.vlastnik === User.id);
export const Domy =
  User &&
  domy.filter((dum) =>
    User.permission === 2
      ? dum.spravce === User.id
      : User.permission === 1
      ? dum.predseda === User.id
      : User.permission === 3
      ? dum
      : null
  );

export const Jednotky =
  Domy &&
  jednotky.filter((jednotka) =>
    Domy.map((dum) => dum.cislo).includes(jednotka.dum)
  );

export const Osoby =
  Jednotky &&
  organizations.filter((org) =>
    Jednotky.map((jedn) => jedn.vlastnik).includes(org.id)
  );

export const Merice =
  User &&
  (User.permission === 3
    ? merice
    : User.permission === 2
    ? merice.filter((meric) =>
        jednotky
          .filter((jednotka) =>
            domy
              .filter((dum) => dum.spravce === User.id)
              .map((dum) => dum.cislo)
              .includes(jednotka.dum)
          )
          .map((jed) => jed.id)
          .includes(meric.jednotka)
      )
    : User.permission === 1
    ? merice.filter((meric) =>
        jednotky
          .filter((jednotka) =>
            domy
              .filter((dum) => dum.predseda === User.id)
              .map((dum) => dum.cislo)
              .includes(jednotka.dum)
          )
          .map((jed) => jed.id)
          .includes(meric.jednotka)
      )
    : User &&
      (Jednotka
        ? // eslint-disable-next-line array-callback-return
          merice.filter((meric) => meric.jednotka === Jednotka.id)
        : null));

const StudenaVodaCelkem = Merice?.filter(
  (meric) => meric.druh === 1
).reduce((a, b) => a + b.odecty[b.odecty.length - 1].ks, 0);
export const TeplaVodaCelkem = Merice?.filter(
  (meric) => meric.druh === 2
).reduce((a, b) => a + b.odecty[b.odecty.length - 1].ks, 0);
export const TeploCelkem = Merice?.filter((meric) => meric.druh === 3).reduce(
  (a, b) => a + b.odecty[b.odecty.length - 1].ks,
  0
);

export function getStudenaCelkem() {
    return StudenaVodaCelkem;
}

export const StudenaVodaSumAbsolut: number[] = [];
export const TeplaVodaSumAbsolut: number[] = [];
export const TeploSumAbsolut: number[] = [];
export const StudenaVodaSumAdds: number[] = [];
export const TeplaVodaSumAdds: number[] = [];
export const TeploSumAdds: number[] = [];

Merice?.map((meric) => {
  meric.odecty
    .map((x) => x.ks)
    .forEach((value, index) => {
      if (meric.druh === 1) {
        if (!StudenaVodaSumAbsolut[index]) {
          StudenaVodaSumAbsolut[index] = value;
        } else {
          StudenaVodaSumAbsolut[index] += value;
        }
      }
      if (meric.druh === 2) {
        if (!TeplaVodaSumAbsolut[index]) {
          TeplaVodaSumAbsolut[index] = value;
        } else {
          TeplaVodaSumAbsolut[index] += value;
        }
      }
      if (meric.druh === 3) {
        if (!TeploSumAbsolut[index]) {
          TeploSumAbsolut[index] = value;
        } else {
          TeploSumAbsolut[index] += value;
        }
      }
    });
});

Merice?.map((meric) => {
  meric.odecty
    .map((x) => x.ks - x.ps)
    .forEach((value, index) => {
      if (meric.druh === 1) {
        if (!StudenaVodaSumAdds[index]) {
          StudenaVodaSumAdds[index] = value;
        } else {
          StudenaVodaSumAdds[index] += value;
        }
      }
      if (meric.druh === 2) {
        if (!TeplaVodaSumAdds[index]) {
          TeplaVodaSumAdds[index] = value;
        } else {
          TeplaVodaSumAdds[index] += value;
        }
      }
      if (meric.druh === 3) {
        if (!TeploSumAdds[index]) {
          TeploSumAdds[index] = value;
        } else {
          TeploSumAdds[index] += value;
        }
      }
    });
});
