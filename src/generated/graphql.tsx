import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Date custom scalar type */
  Date: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};


export type Query = {
  __typename?: 'Query';
  hello?: Maybe<Scalars['String']>;
  dataByToken?: Maybe<RootObject>;
};


export type QueryDataByTokenArgs = {
  token?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  setUserCredentials?: Maybe<Scalars['String']>;
};


export type MutationSetUserCredentialsArgs = {
  token: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
};

export type DruhMerice = {
  __typename?: 'DruhMerice';
  ID?: Maybe<Scalars['Int']>;
  Nazev?: Maybe<Scalars['String']>;
  Jednotka?: Maybe<Scalars['String']>;
};

export type NamereneHodnoty = {
  __typename?: 'NamereneHodnoty';
  ID?: Maybe<Scalars['Int']>;
  DatumOdectu?: Maybe<Scalars['Date']>;
  DatumOd?: Maybe<Scalars['Date']>;
  DatumDo?: Maybe<Scalars['Date']>;
  PocatecniStav?: Maybe<Scalars['Float']>;
  KonecnyStav?: Maybe<Scalars['Float']>;
  Odecet?: Maybe<Scalars['Float']>;
};

export type Merice = {
  __typename?: 'Merice';
  ID?: Maybe<Scalars['Int']>;
  PlatnostOd?: Maybe<Scalars['Date']>;
  PlatnostDo?: Maybe<Scalars['Date']>;
  DatumOvereni?: Maybe<Scalars['Date']>;
  VyrobniCislo?: Maybe<Scalars['String']>;
  Popis?: Maybe<Scalars['String']>;
  DruhMerice?: Maybe<DruhMerice>;
  NamereneHodnoty?: Maybe<Array<Maybe<NamereneHodnoty>>>;
};

export type Sml = {
  __typename?: 'Sml';
  PlatnostOd?: Maybe<Scalars['Date']>;
  PlatnostDo?: Maybe<Scalars['Date']>;
  VariabilniSymbol?: Maybe<Scalars['Int']>;
  ID?: Maybe<Scalars['Int']>;
  Merice?: Maybe<Array<Maybe<Merice>>>;
};

export type Osoby = {
  __typename?: 'Osoby';
  Nazev?: Maybe<Scalars['String']>;
  Ulice?: Maybe<Scalars['String']>;
  UliceSCisly?: Maybe<Scalars['String']>;
  Misto?: Maybe<Scalars['String']>;
  sml?: Maybe<Array<Maybe<Sml>>>;
  Permission?: Maybe<Scalars['String']>;
  Token?: Maybe<Scalars['String']>;
};

export type Jednotky = {
  __typename?: 'Jednotky';
  Oznaceni?: Maybe<Scalars['String']>;
  Popis?: Maybe<Scalars['String']>;
  ID?: Maybe<Scalars['Int']>;
  Osoby?: Maybe<Array<Maybe<Osoby>>>;
};

export type CurrentStats = {
  __typename?: 'CurrentStats';
  Nazev?: Maybe<Scalars['String']>;
  total?: Maybe<Scalars['Float']>;
};

export type Domy = {
  __typename?: 'Domy';
  Oznaceni?: Maybe<Scalars['String']>;
  Popis?: Maybe<Scalars['String']>;
  ID?: Maybe<Scalars['Int']>;
  CurrentStats?: Maybe<Array<Maybe<CurrentStats>>>;
  Jednotky?: Maybe<Array<Maybe<Jednotky>>>;
};

export type RootObject = {
  __typename?: 'RootObject';
  Permission?: Maybe<Scalars['Int']>;
  Token?: Maybe<Scalars['String']>;
  LoginEmail?: Maybe<Scalars['String']>;
  ID?: Maybe<Scalars['Int']>;
  Nazev?: Maybe<Scalars['String']>;
  Ulice?: Maybe<Scalars['String']>;
  UliceSCisly?: Maybe<Scalars['String']>;
  Misto?: Maybe<Scalars['String']>;
  Domy?: Maybe<Array<Maybe<Domy>>>;
};

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}


export type SetLoginCredentialsMutationVariables = Exact<{
  token: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type SetLoginCredentialsMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'setUserCredentials'>
);

export type HelloQueryVariables = Exact<{ [key: string]: never; }>;


export type HelloQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'hello'>
);

export type LoginQueryVariables = Exact<{
  token?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
}>;


export type LoginQuery = (
  { __typename?: 'Query' }
  & { dataByToken?: Maybe<(
    { __typename?: 'RootObject' }
    & Pick<RootObject, 'ID' | 'Permission' | 'Token' | 'Nazev' | 'LoginEmail'>
  )> }
);

export type OdectyQueryVariables = Exact<{
  token?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
}>;


export type OdectyQuery = (
  { __typename?: 'Query' }
  & { dataByToken?: Maybe<(
    { __typename?: 'RootObject' }
    & Pick<RootObject, 'ID' | 'Permission' | 'Token' | 'LoginEmail' | 'Nazev' | 'Ulice' | 'UliceSCisly' | 'Misto'>
    & { Domy?: Maybe<Array<Maybe<(
      { __typename?: 'Domy' }
      & Pick<Domy, 'ID' | 'Oznaceni' | 'Popis'>
      & { CurrentStats?: Maybe<Array<Maybe<(
        { __typename?: 'CurrentStats' }
        & Pick<CurrentStats, 'Nazev' | 'total'>
      )>>>, Jednotky?: Maybe<Array<Maybe<(
        { __typename?: 'Jednotky' }
        & Pick<Jednotky, 'ID' | 'Oznaceni' | 'Popis'>
        & { Osoby?: Maybe<Array<Maybe<(
          { __typename?: 'Osoby' }
          & Pick<Osoby, 'Nazev' | 'Ulice' | 'UliceSCisly' | 'Misto' | 'Token' | 'Permission'>
          & { sml?: Maybe<Array<Maybe<(
            { __typename?: 'Sml' }
            & Pick<Sml, 'ID' | 'PlatnostOd' | 'PlatnostDo' | 'VariabilniSymbol'>
            & { Merice?: Maybe<Array<Maybe<(
              { __typename?: 'Merice' }
              & Pick<Merice, 'ID' | 'PlatnostOd' | 'PlatnostDo' | 'DatumOvereni' | 'Popis' | 'VyrobniCislo'>
              & { DruhMerice?: Maybe<(
                { __typename?: 'DruhMerice' }
                & Pick<DruhMerice, 'ID' | 'Nazev' | 'Jednotka'>
              )>, NamereneHodnoty?: Maybe<Array<Maybe<(
                { __typename?: 'NamereneHodnoty' }
                & Pick<NamereneHodnoty, 'ID' | 'DatumOdectu' | 'DatumOd' | 'DatumDo' | 'PocatecniStav' | 'KonecnyStav' | 'Odecet'>
              )>>> }
            )>>> }
          )>>> }
        )>>> }
      )>>> }
    )>>> }
  )> }
);


export const SetLoginCredentialsDocument = gql`
    mutation setLoginCredentials($token: String!, $email: String!, $password: String!) {
  setUserCredentials(token: $token, email: $email, password: $password)
}
    `;
export type SetLoginCredentialsMutationFn = Apollo.MutationFunction<SetLoginCredentialsMutation, SetLoginCredentialsMutationVariables>;

/**
 * __useSetLoginCredentialsMutation__
 *
 * To run a mutation, you first call `useSetLoginCredentialsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetLoginCredentialsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setLoginCredentialsMutation, { data, loading, error }] = useSetLoginCredentialsMutation({
 *   variables: {
 *      token: // value for 'token'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSetLoginCredentialsMutation(baseOptions?: Apollo.MutationHookOptions<SetLoginCredentialsMutation, SetLoginCredentialsMutationVariables>) {
        return Apollo.useMutation<SetLoginCredentialsMutation, SetLoginCredentialsMutationVariables>(SetLoginCredentialsDocument, baseOptions);
      }
export type SetLoginCredentialsMutationHookResult = ReturnType<typeof useSetLoginCredentialsMutation>;
export type SetLoginCredentialsMutationResult = Apollo.MutationResult<SetLoginCredentialsMutation>;
export type SetLoginCredentialsMutationOptions = Apollo.BaseMutationOptions<SetLoginCredentialsMutation, SetLoginCredentialsMutationVariables>;
export const HelloDocument = gql`
    query hello {
  hello
}
    `;

/**
 * __useHelloQuery__
 *
 * To run a query within a React component, call `useHelloQuery` and pass it any options that fit your needs.
 * When your component renders, `useHelloQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHelloQuery({
 *   variables: {
 *   },
 * });
 */
export function useHelloQuery(baseOptions?: Apollo.QueryHookOptions<HelloQuery, HelloQueryVariables>) {
        return Apollo.useQuery<HelloQuery, HelloQueryVariables>(HelloDocument, baseOptions);
      }
export function useHelloLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<HelloQuery, HelloQueryVariables>) {
          return Apollo.useLazyQuery<HelloQuery, HelloQueryVariables>(HelloDocument, baseOptions);
        }
export type HelloQueryHookResult = ReturnType<typeof useHelloQuery>;
export type HelloLazyQueryHookResult = ReturnType<typeof useHelloLazyQuery>;
export type HelloQueryResult = Apollo.QueryResult<HelloQuery, HelloQueryVariables>;
export const LoginDocument = gql`
    query login($token: String, $email: String, $password: String) {
  dataByToken(token: $token, email: $email, password: $password) {
    ID
    Permission
    Token
    Nazev
    LoginEmail
  }
}
    `;

/**
 * __useLoginQuery__
 *
 * To run a query within a React component, call `useLoginQuery` and pass it any options that fit your needs.
 * When your component renders, `useLoginQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useLoginQuery({
 *   variables: {
 *      token: // value for 'token'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginQuery(baseOptions?: Apollo.QueryHookOptions<LoginQuery, LoginQueryVariables>) {
        return Apollo.useQuery<LoginQuery, LoginQueryVariables>(LoginDocument, baseOptions);
      }
export function useLoginLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<LoginQuery, LoginQueryVariables>) {
          return Apollo.useLazyQuery<LoginQuery, LoginQueryVariables>(LoginDocument, baseOptions);
        }
export type LoginQueryHookResult = ReturnType<typeof useLoginQuery>;
export type LoginLazyQueryHookResult = ReturnType<typeof useLoginLazyQuery>;
export type LoginQueryResult = Apollo.QueryResult<LoginQuery, LoginQueryVariables>;
export const OdectyDocument = gql`
    query odecty($token: String, $email: String, $password: String) {
  dataByToken(token: $token, email: $email, password: $password) {
    ID
    Permission
    Token
    LoginEmail
    Nazev
    Ulice
    UliceSCisly
    Misto
    Domy {
      ID
      Oznaceni
      Popis
      CurrentStats {
        Nazev
        total
      }
      Jednotky {
        ID
        Oznaceni
        Popis
        Osoby {
          Nazev
          Ulice
          UliceSCisly
          Misto
          Token
          Permission
          sml {
            ID
            PlatnostOd
            PlatnostDo
            VariabilniSymbol
            Merice {
              ID
              PlatnostOd
              PlatnostDo
              DatumOvereni
              Popis
              VyrobniCislo
              DruhMerice {
                ID
                Nazev
                Jednotka
              }
              NamereneHodnoty {
                ID
                DatumOdectu
                DatumOd
                DatumDo
                PocatecniStav
                KonecnyStav
                Odecet
              }
            }
          }
        }
      }
    }
  }
}
    `;

/**
 * __useOdectyQuery__
 *
 * To run a query within a React component, call `useOdectyQuery` and pass it any options that fit your needs.
 * When your component renders, `useOdectyQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOdectyQuery({
 *   variables: {
 *      token: // value for 'token'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useOdectyQuery(baseOptions?: Apollo.QueryHookOptions<OdectyQuery, OdectyQueryVariables>) {
        return Apollo.useQuery<OdectyQuery, OdectyQueryVariables>(OdectyDocument, baseOptions);
      }
export function useOdectyLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<OdectyQuery, OdectyQueryVariables>) {
          return Apollo.useLazyQuery<OdectyQuery, OdectyQueryVariables>(OdectyDocument, baseOptions);
        }
export type OdectyQueryHookResult = ReturnType<typeof useOdectyQuery>;
export type OdectyLazyQueryHookResult = ReturnType<typeof useOdectyLazyQuery>;
export type OdectyQueryResult = Apollo.QueryResult<OdectyQuery, OdectyQueryVariables>;