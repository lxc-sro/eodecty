import styled from '../app/styled';
import Text from './Text';

const Box = styled(Text).attrs(() => ({ as: 'div' }))`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
`;

export default Box;
