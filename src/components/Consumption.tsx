import { colors } from "../app/theme";
import React from "react";
import styled from "../app/styled";
import Box from "./Box";
import Text from "./Text";

const Consumption = (props: {
  name: string;
  value: number | string | null |undefined;
  unit?: string;
  color?: keyof typeof colors;
  onClickAction?: () => void;
}) => {
  return (
    <Wrapper onClick={props.onClickAction} color={props.color}>
      <ConsumptionName>{props.name}</ConsumptionName>
      <ConsumptionValues>
        <ConsumptionValue>{props.value}</ConsumptionValue>
        <ConsumptionUnit>{props.unit}</ConsumptionUnit>
      </ConsumptionValues>
    </Wrapper>
  );
};

const Wrapper = styled(Box)<{ color: keyof typeof colors }>`
  position: relative;
  flex: 0 0 30%;
  border-bottom: 3px solid ${(props) => props.theme.colors[props.color]};
  background: ${(props) => props.theme.colors.white};
  padding: ${(props) => props.theme.spacing.default};
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  cursor: pointer;

  & > * {
    cursor: pointer;
  }
`;

const ConsumptionName = styled(Text)`
  font-size: ${(props) => props.theme.fontSize.normal}px;
  font-weight: 700;
`;

const ConsumptionValues = styled(Box)`
  flex-direction: row;
  flex-wrap: wrap;
`;

const ConsumptionValue = styled(Text)`
  font-size: ${(props) => props.theme.fontSize.medium}px;
  font-weight: 700;
  padding-right: ${(props) => props.theme.spacing.small};
`;
const ConsumptionUnit = styled(Text)`
  font-style: italic;
`;

export default Consumption;
