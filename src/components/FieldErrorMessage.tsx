import styled from 'styled-components';

const FieldErrorMessage = styled.div`
  color: #bf2700;
  font-size: 12px;
  margin-top: 3px;
`;

export default FieldErrorMessage;
