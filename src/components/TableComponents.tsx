import styled from "../app/styled";

export const TableRow = styled.tr`
  color: ${(props) => props.theme.colors.grey};
  &:first-child {
    border-bottom: 2px solid ${(props) => props.theme.colors.grey};
    font-weight: 700;
    &:hover {
      background: transparent;
    }
  }

  &:hover {
    background: ${(props) => props.theme.colors.white};
  }
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

export const TableCol = styled.td`
  padding: ${(props) => props.theme.spacing.small};
`;
