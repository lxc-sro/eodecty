import { createContext } from "react";

export interface UserContextInterface {
  id?: string;
  token?: string;
  permission?: number;
  name?: string;
}

const UserContext = createContext<UserContextInterface>({
  id: null,
  name: null,
  token: null,
  permission: null,
});

export default UserContext;
