import React from "react";
import styled from "styled-components";

const cloudimgBaseUrl = "https://apoxmpcimo.cloudimg.io";

type Props = {
    src: string;
    alt: string;
    size?: number;
    noHover?: boolean;
    itemProp?: string;
  };

  const Image = (props: Props) => (
      <div className="image-wrapper">
        <Img
          src={`${cloudimgBaseUrl}/width/${props.size ? props.size : 500}/n/${props.src}`}
          alt={props.alt}
          itemProp={props.itemProp}
          noHover={props.noHover}
        />
      </div>
  );
  
  const Img = styled.img<{ noHover?: boolean }>`
    width: 100%;
    height: auto;
    line-height: 0;
    transition: all 0.5s ease;
  
    &:hover {
      transform: ${({ noHover }) => (noHover ? null : 'scale(1.1)')};
      opacity: ${({ noHover }) => (noHover ? 1 : 0.9)};
    }
  `;
  
  export default Image;