import React from "react";
import Spinner from "@atlaskit/spinner";
import Flex from "./Flex";
import Menu from "./Menu";
import styled from "../app/styled";
import Box from "./Box";
import Footer from "./Footer";

const Page = (props: {
  title?: string;
  children?: any;
  isLoading?: boolean;
  isError?: boolean;
  actions?: any;
}) => (
  <StyledBox>
    <Menu />
    <Wrapper>
      <Flex>
        {props.title && <Title>{props.title}</Title>}
        <ActionsWrapper>{props.actions}</ActionsWrapper>
      </Flex>

      {props.isLoading && (
        <SpinnerWrapper>
          <Spinner size="large" />
        </SpinnerWrapper>
      )}
      {props.isError && "Error"}
      {!props.isLoading && !props.isError && props.children}
    </Wrapper>
    <Footer />
  </StyledBox>
);

const StyledBox = styled(Box)`
  background: ${(props) => props.theme.colors.lightGrey};
`;
const SpinnerWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ActionsWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const Wrapper = styled.div`
  max-width: 1000px;
  width: 100%;
  margin: 0 auto;
  padding: 40px 20px;
  background: ${(props) => props.theme.colors.lightGrey};
  @media only screen and (max-width: 850px) {
    margin-top: ${(props) => props.theme.spacing.max};
  }
`;

const Title = styled.h1`
  margin-bottom: 30px;
  font-size: 30px;
`;

export default Page;
