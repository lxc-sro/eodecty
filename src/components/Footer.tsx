import React from "react";
import Box from "./Box";
import Text from "./Text";
import styled from "../app/styled";

const Footer = () => {
  return (
    <Row>
      <Text>Vytvořil PROFITHERM CZ &copy;</Text>
      <img
        alt="Profitherm logo"
        src="/logo-profitherm.png"
        height="80px"
      />
    </Row>
  );
};

const Row = styled(Box)`
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-around;
  background: ${props => props.theme.colors.white};
`;

export default Footer;
