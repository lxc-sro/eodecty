import cookies from "js-cookie";
import React, { useState } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import Icon from "./Icon";
import { colors } from "../app/theme";
import styled from "../app/styled";
import Box from "./Box";
import Text from "./Text";
import { useLoginQuery } from "../generated/graphql";

const Menu = ({ history, match }: RouteComponentProps) => {
  const signOut = () => {
    cookies.remove("token");
    history.push("/login");
  };

  const { data, loading, error } = useLoginQuery({
    variables: { token: cookies.get("token") },
  });

  const User = !loading && !error && data && data.dataByToken;

  const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);

  return (
    <Container>
      <Wrapper>
        <Items isMobileMenuOpen={isMobileMenuOpen}>
          <Item>
            <NavItem
              onClick={() => history.push("/dashboard")}
              active={match.path.includes("/dashboard")}
            >
              <Icon
                name="country-house"
                color={isMobileMenuOpen ? "#000000" : "#ffffff"}
                style={{ marginRight: 10 }}
              />
              Přehled
            </NavItem>
          </Item>
          {
            // @ts-ignore
            User.Permission > 1 && (
              <Item>
                <NavItem
                  onClick={() => history.push("/domy")}
                  active={match.path.includes("/domy")}
                >
                  <Icon
                    name="real-estate"
                    color={isMobileMenuOpen ? "#000000" : "#ffffff"}
                    style={{ marginRight: 10 }}
                  />
                  Domy
                </NavItem>
              </Item>
            )
          }
          {
            //@ts-ignore
            User.Permission > 0 && (
              <Item>
                <NavItem
                  onClick={() => history.push("/vlastnici")}
                  active={match.path.includes("/vlastnici")}
                >
                  <Icon
                    name="organization-chart-people"
                    color={isMobileMenuOpen ? "#000000" : "#ffffff"}
                    style={{ marginRight: 10 }}
                  />
                  Vlastníci
                </NavItem>
              </Item>
            )
          }
          <Item>
            <NavItem
              onClick={() => history.push("/odecty")}
              active={match.path.includes("/odecty")}
            >
              <Icon
                name="speed"
                color={isMobileMenuOpen ? "#000000" : "#ffffff"}
                style={{ marginRight: 10 }}
              />
              Odečty
            </NavItem>
          </Item>
          <Item>
            <NavItem
              onClick={() => history.push("/nastaveni")}
              active={match.path.includes("/nastaveni")}
            >
              Nastavení
            </NavItem>
          </Item>
          <Item>
            <NavItem onClick={() => signOut()}>
              <Icon
                name="logout-rounded"
                color={isMobileMenuOpen ? "#000000" : "#ffffff"}
                style={{ marginRight: 10 }}
              />
              Odhlásit
            </NavItem>
          </Item>
        </Items>
        <Name>{User.Nazev}</Name>
        <Toggle>
          <Box onClick={() => setMobileMenuOpen(!isMobileMenuOpen)}>
            <Icon
              name={isMobileMenuOpen ? "delete-sign" : "menu"}
              color="#ffffff"
            />
          </Box>
        </Toggle>
      </Wrapper>
    </Container>
  );
};

const Wrapper = styled.ul`
  list-style-type: none;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
`;

const Items = styled(Box)<{ isMobileMenuOpen: boolean }>`
  text-align: center;
  flex-direction: row;
  justify-content: flex-start;
  order: 2;
  align-items: center;

  @media only screen and (max-width: 850px) {
    background: ${(props) => props.theme.colors.white};
    position: fixed;
    overflow: scroll;
    top: 0;
    left: 0;
    width: ${(props) => (props.isMobileMenuOpen ? "80%" : "100%")};
    height: 100vh;
    flex-direction: column;
    align-items: center;
    transform: ${(props) =>
      props.isMobileMenuOpen ? "translateX(25%)" : "translateX(100%)"};
    transition: transform 0.3s ease-in-out;
    box-shadow: 0 0px 12px 6px hsla(0, 0%, 0%, 0.5);
  }
`;

const Item = styled.li`
  position: relative;
  display: block;
  width: auto;
  align-items: center;

  @media only screen and (max-width: 850px) {
    width: 100%;
  }
`;

const Toggle = styled.li`
  padding: ${(props) => props.theme.spacing.default};
  position: fixed;
  right: 5px;
  display: none;
  width: auto;
  order: 1;
  z-index: 101;

  @media only screen and (max-width: 850px) {
    display: block;
  }
`;

const Container = styled(Box)`
  position: relative;
  top: 0;
  z-index: 10;
  width: 100%;
  background: ${(props) => props.theme.colors.red};
  z-index: 100;
  @media only screen and (max-width: 850px) {
    position: fixed;
    top: 0px;
    margin-bottom: ${(props) => props.theme.spacing.default};
  }
`;

const Name = styled(Text)`
  color: ${(props) => props.theme.colors.white};
  padding: 18px 20px;
  text-transform: uppercase;

  @media only screen and (max-width: 850px) {
    padding: ${(props) => props.theme.spacing.default};
  }
`;

const NavItem = styled(Text)<{
  noMargin?: boolean;
  color?: keyof typeof colors;
  active?: boolean;
}>`
  cursor: pointer;
  color: ${(props) => props.theme.colors.white};
  text-transform: uppercase;
  transition: all 300ms;
  background: ${(props) => (props.active ? "rgba(0,0,0, 0.2)" : "transparent")};
  padding: 18px 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: ${(props) => (props.noMargin ? 0 : props.theme.spacing.big)};

  @media only screen and (max-width: 850px) {
    color: ${({ theme }) => theme.colors.black};
    display: flex;
    padding: ${(props) => props.theme.spacing.default};
    margin-left: 0;
  }

  &:hover {
    background: rgba(0, 0, 0, 0.2);
  }
`;

export default withRouter(Menu);
