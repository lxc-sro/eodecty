import React from "react";
import cookies from "js-cookie";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  RouteProps,
  Switch,
} from "react-router-dom";
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import { theme } from "./app/theme";
import { ThemeProvider } from "./app/styled";
import GlobalStyle from "./app/global";
import LoginPage from "./pages/login";
import DashboardPage from "./pages/dashboard";
import OwnersPage from "./pages/owners";
import BuildingsPage from "./pages/buildings";
import ConsumptionPage from "./pages/consumption";
import SettingsPage from "./pages/settings";
import CreateConsumptionPage from "./pages/createConsumption";
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client";
import UserContext from "./components/UserContext";
import { LOGIN } from "./graphql/queries";

const client = new ApolloClient({
  uri: "/api/graphql",
  cache: new InMemoryCache(),
});

const UserRoute = ({
  component: Component,
  ...rest
}: { component: React.ComponentType<RouteProps> } & RouteProps) => {
  return (
    <Route
      {...rest}
      render={(props: any) =>
        cookies.get("token") ? (
          // @ts-ignore
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

class MyApp extends React.Component {
  constructor(props: any) {
    super(props);
    this.state = {
      id: null,
      token: null,
      permission: null,
      name: null,
    };
  }

  async componentDidMount() {
    const token = cookies.get("token");

    if (token) {
      await client
        .query({ query: LOGIN, variables: { token: token } })
        .then((data) => {
          this.setState({
            id: data.data.dataByToken.ID,
            token: data.data.dataByToken.Token,
            permission: data.data.dataByToken.Permission,
            name: data.data.dataByToken.Nazev,
          });
        })
        .catch(() => {
          cookies.remove("token");
        });
    }
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
          <UserContext.Provider value={this.state}>
            <GlobalStyle />
            <Router>
              <Switch>
                <Route
                  exact
                  path="/"
                  component={() => <Redirect to="/dashboard" />}
                />
                <Route exact path="/login" component={LoginPage} />
                <UserRoute exact path="/dashboard" component={DashboardPage} />
                <UserRoute exact path="/domy" component={BuildingsPage} />
                <UserRoute exact path="/vlastnici" component={OwnersPage} />
                <UserRoute exact path="/odecty" component={ConsumptionPage} />
                <UserRoute exact path="/nastaveni" component={SettingsPage} />
                <UserRoute
                  exact
                  path="/odecty/pridat"
                  component={CreateConsumptionPage}
                />
                <Route component={() => <p>#404</p>} />
              </Switch>
            </Router>
          </UserContext.Provider>
        </ThemeProvider>
      </ApolloProvider>
    );
  }
}

export default MyApp;
