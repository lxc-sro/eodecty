import React from "react";
import Page from "../components/Page";
import { withRouter } from "react-router-dom";
import styled from "../app/styled";
import Box from "../components/Box";
import Consumption from "../components/Consumption";
import cookies from "js-cookie";
import { useLoginQuery } from "../generated/graphql";

const DashboardPage = () => {

  const { data, loading, error } = useLoginQuery({
    variables: { token: cookies.get("token") },
  });
  if (loading || error) {
    return (
      <Page>
        <p>loading</p>
      </Page>
    );
  }

  return (
    <Page title="E-odečty">
      <Consumptions>
        <Consumption name="Uživatel" value={data.dataByToken.Nazev} />
        <Consumption name="Oprávění" value={data.dataByToken.Permission} />
      </Consumptions>
    </Page>
  );
};

const Consumptions = styled(Box)`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export default withRouter(DashboardPage);
