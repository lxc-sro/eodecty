import React from "react";
import Page from "../components/Page";
import { Formik, Form } from "formik";
import Flex from "../components/Flex";
import TextField from "../components/TextField";
import * as Yup from "yup";
import { withRouter } from "react-router-dom";

import { ToastContainer } from "react-toastify";
import { Button } from "@atlaskit/button/dist/cjs/components/Button";

const Schema = Yup.object().shape({
  vs: Yup.string()
    .min(2, "Zadejte minimálně 2 znaky")
    .max(200, "Toto pole je dlouhé. Zadejte maximálně 200 znaků.")
    .required("Toto pole je povinné."),
  ks: Yup.number()
    .min(2, "Zadejte minimálně 2 znaky")
    .max(200, "Toto pole je dlouhé. Zadejte maximálně 200 znaků.")
    .required("Toto pole je povinné."),
});

interface FormValues {
    vc: string;
    ks: number;
  }

const CreateConsumptionPage = () => {
    const initialValues: FormValues = { vc: '', ks: 0 };

  return (
    <Page title="Samoodečet">
      <ToastContainer enableMultiContainer position={"top-right"} />

      <Formik
        initialValues={initialValues}
        validationSchema={Schema}
        onSubmit={(values: FormValues) => {
            console.log(values)
        }}
      >
        {(props) => {
          return (
            <Form>
              <Flex>
                <TextField
                  formikProps={props}
                  name="vc"
                  label="Výrobní číslo měřiče"
                  placeholder="Výrobní číslo měřiče"
                  isRequired
                />
                <TextField
                  formikProps={props}
                  name="ks"
                  label="Konečný stav"
                  placeholder="Konečný stav"
                  isRequired
                />
              </Flex>
              <div style={{ marginTop: 20 }}>
                <Button
                  type="submit"
                  isDisabled={!props.isValid}
                  appearance="primary"
                >
                  Uložit
                </Button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </Page>
  );
};

export default withRouter(CreateConsumptionPage);
