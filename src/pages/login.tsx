import { Formik } from "formik";
import React, { useState } from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import Button from "../components/Button";
import Spinner from "@atlaskit/spinner";
import TextField from "@atlaskit/textfield";
import cookies from "js-cookie";
import styled from "../app/styled";
import Box from "../components/Box";
import Text from "../components/Text";
import Image from "../components/Image";
import { toast, ToastContainer } from "react-toastify";
import { useOdectyLazyQuery, useLoginLazyQuery } from "../generated/graphql";

const LoginPage = ({ history }: RouteComponentProps) => {
  const [executeLoginQuery, loginQuery] = useLoginLazyQuery();

  const [showEmail, setShowEmail] = useState(false);

  // if (loginQuery.called && loginQuery.data?.dataByToken) {
  //   cookies.set("user", loginQuery.data.dataByToken);
  //   cookies.set("token", JSON.stringify(loginQuery.variables?.token));
  //   history.push("/dashboard");
  // } else {
  //   loginQuery.called = false;
  //   toast.error("Špatné heslo");
  //   setIsSubmiting(false);
  // }

  if (loginQuery.called && loginQuery.data) {
    if (loginQuery.data.dataByToken !== null) {
      toast.success("Uživatel přihlášen");
      //cookies.set("user", JSON.stringify(loginQuery.data.dataByToken));
      //console.log(loginQuery.data.dataByToken);
      cookies.set("token", loginQuery.data.dataByToken.Token);
      history.push("/dashboard");
    } else {
      //@ts-ignore
      loginQuery.called = false;
      toast.warning("Špatné přihlašovací údaje");
    }
  }

  return (
    <>
      <ToastContainer enableMultiContainer position={"top-right"} />
      <MainImage
        alt="Profitherm logo"
        src="/logo-profitherm.png"
        height="50px"
      />
      <Row>
        <ImageWrapper>
          <Image
            alt="E-odečty Profitherm"
            size={1000}
            noHover
            src="https://res.cloudinary.com/dqidmavva/image/upload/v1598982732/obrazek_lp_bwsmdw.png"
          />
        </ImageWrapper>
        <FormWrapper>
          <TextBox>
            <Heading as="h1">E-odečty</Heading>
            <Subheading as="h2">od Profithermu</Subheading>
            <StyledText as="p">
              Jako uživatel vidíte svou aktuální spotřebu jednotlivých služeb.
              Díky grafům můžete porovnávat spotřeby ve více období.
            </StyledText>
            <StyledText as="p">
              Mějte přehled o jednotlivých domech, správcích a nájemnících.
            </StyledText>
            <StyledText as="p">
              Chcete si E-odečty vyzkoušet? Připravili jsme pro Vás{" "}
              <a href="https://demo.profitherm.cz" style={{ color: "#DA291C" }}>
                demoverzi
              </a>
            </StyledText>
          </TextBox>
          <Form>
            {loginQuery.called && (
              <SubmitingLoader>
                <Spinner />
              </SubmitingLoader>
            )}
            <Row
              style={{
                justifyContent: "space-between",
                alignItems: "center",
                paddingBottom: "10px",
              }}
            >
              <Button
                color="red"
                type="button"
                onClick={() => setShowEmail(false)}
              >
                Přihlásit heslem
              </Button>
              <Button
                color="red"
                type="button"
                onClick={() => setShowEmail(true)}
              >
                Přihlásit emailem
              </Button>
            </Row>
            <Formik
              initialValues={{ pass: "", email: "", password: "" }}
              onSubmit={async (values) => {
                //const found = orgs.find((org) => org.token === values.pass);
                if (
                  values.pass === "" &&
                  values.email === "" &&
                  values.password === ""
                ) {
                  toast.warning("Musíte zadat přihlašovací údaje");
                } else {
                  await executeLoginQuery({
                    variables: {
                      token: values.pass,
                      email: values.email,
                      password: values.password,
                    },
                  });
                }

                // if (found) {
                //   cookies.set("user", found);
                //   cookies.set("token", values.pass);
                //   history.push("/dashboard");
                // } else {
                //   toast.error("Špatné heslo");
                //   setIsSubmiting(false);
                // }

                //await login({ variables: { ...values, adminPanel: true } });
              }}
              render={(props) => (
                <form onSubmit={props.handleSubmit}>
                  {showEmail ? (
                    <>
                      <TextField
                        type="email"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.email}
                        name="email"
                        label="Email"
                        placeholder="Zadejte přihlašovací email"
                        required
                      />
                      <br />
                      <TextField
                        type="password"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.password}
                        name="password"
                        label="Heslo"
                        placeholder="Zadejte vaše heslo"
                        required
                      />
                    </>
                  ) : (
                    <TextField
                      type="password"
                      onChange={props.handleChange}
                      onBlur={props.handleBlur}
                      value={props.values.pass}
                      name="pass"
                      label="Heslo"
                      placeholder="Zadejte přihlašovací heslo"
                      required
                    />
                  )}

                  <Row
                    style={{
                      justifyContent: "space-between",
                      alignItems: "center",
                      paddingTop: "10px",
                    }}
                  >
                    <TextFlexible as="p">
                      Své přihlašovací heslo naleznete na vyúčtování, případně u
                      svého předsedy či správce.
                    </TextFlexible>
                    <Button color="black" type="submit">
                      Přihlásit se
                    </Button>
                  </Row>
                </form>
              )}
            />
          </Form>
        </FormWrapper>
      </Row>

      {/*error && (
        <div style={{ marginTop: 50 }}>
          <SectionMessage appearance="error">
            Uživatel s vámi zadanou emailovou adresou neexistuje nebo nemá oprávnění administrátora.
          </SectionMessage>
        </div>
      )*/}
    </>
  );
};

const Row = styled(Box)`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: center;
  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const TextFlexible = styled(Text)`
  color: ${(props) => props.theme.colors.white};
  flex: 0 0 70%;
  @media only screen and (max-width: 768px) {
    flex: 0 0 100%;
  }
`;

const ImageWrapper = styled(Box)`
  flex: 0 0 45%;
  @media only screen and (max-width: 768px) {
    flex: 0 0 100%;
  }
`;

const FormWrapper = styled(Box)`
  flex: 0 0 45%;
  @media only screen and (max-width: 768px) {
    flex: 0 0 100%;
  }
`;

const MainImage = styled.img`
  padding: 0 ${(props) => props.theme.spacing.default};
`;

const SubmitingLoader = styled(Box)`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background: rgba(0, 0, 0, 0.4);
  z-index: 2;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
`;

const Heading = styled(Text)`
  color: ${({ theme }) => theme.colors.red};
  font-size: ${({ theme }) => theme.fontSize.extreme}px;
  font-weight: 700;
  text-transform: uppercase;
  display: block;
`;

const Subheading = styled(Text)`
  color: ${({ theme }) => theme.colors.grey};
  font-size: ${({ theme }) => theme.fontSize.max}px;
  font-weight: 700;
  display: block;
`;

const Form = styled(Box)`
  background: ${(props) => props.theme.colors.red};
  padding: ${(props) => props.theme.spacing.max};
  padding-top: 10px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  border-radius: 4px;
  position: relative;
`;

const TextBox = styled(Box)`
  flex: 0 0 40%;
  padding: ${(props) => props.theme.spacing.default};
  text-align: right;
`;

const StyledText = styled(Text)`
  font-size: ${(props) => props.theme.fontSize.medium}px;
  color: ${(props) => props.theme.colors.grey};
  align-items: center;
  padding: ${(props) => props.theme.spacing.small};
  display: block;
`;

export default withRouter(LoginPage);
