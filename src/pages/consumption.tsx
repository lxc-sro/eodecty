import React, { useState } from "react";
import Page from "../components/Page";
import Box from "../components/Box";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Table, TableRow, TableCol } from "../components/TableComponents";
import cookies from "js-cookie";
import { useOdectyQuery } from "../generated/graphql";
import styled from "../app/styled";
import Select from "react-select";

const ConsumptionPage = ({ history }: RouteComponentProps) => {
  const [showRow, setShowRow] = useState(0);
  const [selectedDum, setSelectedDum] = useState(0);
  const [selectedJednotka, setSelectedJednotka] = useState(0);
  const [selectedOsoba, setSelectedOsoba] = useState("");

  const user = cookies.get("token");

  const { data, loading, error } = useOdectyQuery({
    variables: { token: user },
  });

  if (loading || error || !data.dataByToken.Domy) {
    return <Page title="Odečty">Načítání</Page>;
  }

  return (
    <Page title="Odečty">
      <SearchRow>
        <SearchBox>
          <Select
            options={data.dataByToken?.Domy?.filter((dum) =>
              selectedDum > 0 ? dum.ID === selectedDum : dum.ID > 0
            )?.flatMap((dum) =>
              dum.Jednotky?.filter((jednotka) =>
                selectedJednotka > 0
                  ? jednotka.ID === selectedJednotka
                  : jednotka.ID > 0
              )?.flatMap((jednotka) =>
                jednotka.Osoby?.flatMap((osoba) => {
                  return { value: osoba.Token, label: osoba.Nazev };
                })
              )
            )}
            onChange={(val) => {
              if (!val) {
                setSelectedOsoba("");
              } else {
                setSelectedOsoba(val.value);
              }
            }}
            isDisabled={selectedJednotka === 0}
            isClearable
            placeholder="Vyberte vlastníka"
          />
        </SearchBox>
        <SearchBox>
          <Select
            options={data.dataByToken.Domy.filter((dum) =>
              selectedDum > 0 ? dum.ID === selectedDum : dum.ID !== null
            )?.flatMap((dum) =>
              dum.Jednotky?.flatMap((jednotka) => {
                return { value: jednotka.ID, label: jednotka.Oznaceni };
              })
            )}
            onChange={(val) => {
              if (!val) {
                setSelectedJednotka(0);
              } else {
                setSelectedJednotka(val.value);
              }
              setSelectedOsoba("");
            }}
            isDisabled={selectedDum === 0}
            isClearable
            placeholder="Vyberte byt"
          />
        </SearchBox>
        <SearchBox>
          <Select
            options={data.dataByToken.Domy.flatMap((dum) => {
              return { value: dum.ID, label: dum.Popis };
            })}
            onChange={(val) => {
              if (!val) {
                setSelectedDum(0);
              } else {
                setSelectedDum(val.value);
              }
              setSelectedJednotka(0);
              setSelectedOsoba("");
            }}
            isClearable
            placeholder="Vyberte dům"
          />
        </SearchBox>
      </SearchRow>

      <Table>
        <TableRow>
          <TableCol>Byt</TableCol>
          <TableCol>Nájemník</TableCol>
          <TableCol>Výrobní číslo</TableCol>
          <TableCol>Druh</TableCol>
          <TableCol>Měrná jednotka</TableCol>
          <TableCol>Aktuální stav</TableCol>
        </TableRow>
        {(loading || error) && <TableRow>Načítám data</TableRow>}

        {data.dataByToken.Domy.filter((dum) =>
          selectedDum > 0 ? dum.ID === selectedDum : dum.ID > 0
        )?.map((domy) =>
          domy.Jednotky?.filter((jednotka) =>
            selectedJednotka > 0
              ? jednotka.ID === selectedJednotka
              : jednotka.ID > 0
          )?.map((jednotka) =>
            jednotka.Osoby?.filter((osoba) =>
              selectedOsoba !== ""
                ? osoba.Token === selectedOsoba
                : osoba !== null
            )?.map((osoba) =>
              osoba.sml?.map((smlouva) =>
                smlouva.Merice?.map((meric) => {
                  return (
                    <>
                      <TableRow
                        onClick={() =>
                          showRow === meric.ID
                            ? setShowRow(0)
                            : setShowRow(meric.ID)
                        }
                      >
                        <TableCol>Byt {jednotka.Oznaceni}</TableCol>
                        <TableCol>{osoba.Nazev}</TableCol>
                        <TableCol>{meric.VyrobniCislo}</TableCol>
                        <TableCol>{meric.DruhMerice?.Nazev}</TableCol>
                        <TableCol>{meric.DruhMerice?.Jednotka}</TableCol>
                        <TableCol>
                          {meric.NamereneHodnoty
                            ? meric.DruhMerice.Jednotka === "kWh"
                              ? meric.NamereneHodnoty[
                                  meric.NamereneHodnoty?.length > 0
                                    ? meric.NamereneHodnoty.length - 1
                                    : 0
                                ]?.KonecnyStav / 1000
                              : meric.NamereneHodnoty[
                                  meric.NamereneHodnoty?.length > 0
                                    ? meric.NamereneHodnoty.length - 1
                                    : 0
                                ]?.KonecnyStav
                            : 0}{" "}
                          {meric.DruhMerice?.Jednotka}
                        </TableCol>
                      </TableRow>
                      {showRow === meric.ID && (
                        <TableRow>
                          <TableCol colSpan={5}>
                            {meric.NamereneHodnoty ? (
                              <Table>
                                <TableRow>
                                  <TableCol>Datum odečtu</TableCol>
                                  <TableCol>Počáteční stav</TableCol>
                                  <TableCol>Konečný stav</TableCol>
                                  <TableCol>Spotřeba</TableCol>
                                </TableRow>
                                {meric.NamereneHodnoty.map(
                                  (odecet, index, arr) => (
                                    <TableRow>
                                      <TableCol>
                                        {new Date(
                                          odecet.DatumOdectu
                                        ).toLocaleDateString()}
                                      </TableCol>
                                      <TableCol>
                                        {meric.DruhMerice?.Jednotka === "kWh"
                                          ? arr[index > 1 ? index - 1 : 0]
                                              .KonecnyStav / 1000
                                          : arr[index > 1 ? index - 1 : 0]
                                              .KonecnyStav}{" "}
                                        {meric.DruhMerice?.Jednotka}
                                      </TableCol>
                                      <TableCol>
                                        {meric.DruhMerice?.Jednotka === "kWh"
                                          ? odecet.KonecnyStav / 1000
                                          : odecet.KonecnyStav}{" "}
                                        {meric.DruhMerice?.Jednotka}
                                      </TableCol>
                                      <TableCol>
                                        {Math.round(
                                          meric.DruhMerice?.Jednotka === "kWh"
                                            ? (odecet.KonecnyStav / 1000) *
                                                100 -
                                                (arr[index > 1 ? index - 1 : 0]
                                                  .KonecnyStav /
                                                  1000) *
                                                  100
                                            : odecet.KonecnyStav * 100 -
                                                arr[index > 1 ? index - 1 : 0]
                                                  .KonecnyStav *
                                                  100
                                        ) / 100}{" "}
                                        {meric.DruhMerice?.Jednotka}
                                      </TableCol>
                                    </TableRow>
                                  )
                                )}
                              </Table>
                            ) : (
                              <strong>Nejsou odečty</strong>
                            )}
                          </TableCol>
                        </TableRow>
                      )}
                    </>
                  );
                })
              )
            )
          )
        )}
      </Table>
    </Page>
  );
};

const SearchRow = styled(Box)`
  flex-direction: row;
`;

const SearchBox = styled(Box)`
  width: 100%;
  padding: 0 ${({ theme }) => theme.spacing.default};
`;
export default withRouter(ConsumptionPage);
