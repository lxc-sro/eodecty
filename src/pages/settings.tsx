import React from "react";
import Page from "../components/Page";
import { Formik, Form } from "formik";
import Flex from "../components/Flex";
import TextField from "../components/TextField";
import * as Yup from "yup";
import cookies from "js-cookie";
import {
  useLoginQuery,
  useSetLoginCredentialsMutation,
} from "../generated/graphql";

import { toast, ToastContainer } from "react-toastify";
import { Button } from "@atlaskit/button/dist/cjs/components/Button";

const Schema = Yup.object().shape({
  email: Yup.string()
    .min(2, "Zadejte minimálně 2 znaky")
    .max(200, "Toto pole je dlouhé. Zadejte maximálně 200 znaků.")
    .required("Toto pole je povinné."),
  pass: Yup.string()
    .min(2, "Zadejte minimálně 2 znaky")
    .max(200, "Toto pole je dlouhé. Zadejte maximálně 200 znaků.")
    .required("Toto pole je povinné."),
});

interface FormValues {
  email: string;
  pass: string;
}

const SettingsPage = () => {
  const user = cookies.get("token");

  const { data, loading, error } = useLoginQuery({
    variables: { token: user },
  });

  const [executeSetLogin, setLogin] = useSetLoginCredentialsMutation();

  const initialValues: FormValues = { email: "", pass: "" };

  if (loading || error || !data.dataByToken) {
    return <Page title="Nastavení">Načítání</Page>;
  }

  initialValues.email = data.dataByToken?.LoginEmail;

  if (
    setLogin.called &&
    setLogin.data &&
    setLogin.data.setUserCredentials === "OK"
  ) {
    toast.success("Email a heslo nastaveno");
  }

  return (
    <Page title="Nastavení">
      <ToastContainer enableMultiContainer position={"top-right"} />

      <Formik
        initialValues={initialValues}
        validationSchema={Schema}
        onSubmit={async (values: FormValues) => {
          await executeSetLogin({
            variables: {
              token: user,
              email: values.email,
              password: values.pass,
            },
          });
        }}
      >
        {(props) => {
          return (
            <Form>
              <Flex>
                <TextField
                  formikProps={props}
                  name="email"
                  label="Váš email"
                  placeholder="Váš email"
                  isRequired
                />
                <TextField
                  formikProps={props}
                  name="pass"
                  label="Nové heslo"
                  placeholder="Nové heslo"
                  isRequired
                />
              </Flex>
              <div style={{ marginTop: 20 }}>
                <Button
                  type="submit"
                  isDisabled={!props.isValid && props.values.pass === ""}
                  appearance="primary"
                >
                  Uložit
                </Button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </Page>
  );
};

export default SettingsPage;
