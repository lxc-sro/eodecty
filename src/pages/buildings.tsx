import React, { useState } from "react";
import Page from "../components/Page";
import SearchInput, { searchData } from "../components/SearchInput";
import { Table, TableRow, TableCol } from "../components/TableComponents";
import Chart from "react-apexcharts";
import cookies from "js-cookie";
import { useOdectyQuery } from "../generated/graphql";

const BuildingsPage = () => {
  const [needle, setNeedle] = useState("");

  const user = cookies.get("token");

  const { data, loading, error } = useOdectyQuery({
    variables: { token: user },
  });

  const [selectedDum, setSelectedDum] = useState(0);
  const search =
    !loading && !error && data && data.dataByToken && searchData(data.dataByToken.Domy, ["Oznaceni", "Popis"], needle);
  return (
    <Page title="Domy">
      <SearchInput
        value={needle}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setNeedle(e.target.value)
        }
        onSubmit={() => console.log("asd")}
        placeholder="Zadejte adresu"
      />

      <Table>
        <tbody>
          <TableRow>
            <TableCol>Označení</TableCol>
            <TableCol>Adresa</TableCol>
          </TableRow>
          {(loading || error) && <TableRow>Načítám data</TableRow>}
          {search && search.map((dum) => {
            return (
              <>
                <TableRow
                  onClick={() => {
                    selectedDum === dum.ID
                      ? setSelectedDum(0)
                      : setSelectedDum(dum.ID);
                  }}
                >
                  <TableCol>{dum.Oznaceni}</TableCol>
                  <TableCol>{dum.Popis}</TableCol>
                </TableRow>
                {selectedDum === dum.ID && (
                  <TableRow>
                    <TableCol colSpan={3}>
                      <Table>
                        <tbody>
                          <TableRow>
                            <TableCol>Přehled - {dum.Oznaceni}</TableCol>
                          </TableRow>
                          {dum.CurrentStats ? (
                            <TableRow>
                              <TableCol>
                                <Chart
                                  //@ts-ignore
                                  type="bar"
                                  options={{
                                    xaxis: {
                                      categories: dum.CurrentStats?.flatMap(
                                        (stat) => stat.Nazev
                                      ),
                                    },

                                    tooltip: {
                                      shared: true,
                                    },
                                    dropShadow: {
                                      enabled: true,
                                      top: 0,
                                      left: 0,
                                      blur: 3,
                                      opacity: 0.5,
                                    },
                                    stroke: { curve: "smooth" },
                                  }}
                                  series={dum.CurrentStats?.flatMap((stat) => ({
                                    name: stat.Nazev,
                                    type: "bar",
                                    data: stat.Nazev === "Měřič tepla kWh" ? [stat.total/1000] : [stat.total],
                                    id: stat.Nazev,
                                  }))}
                                />
                              </TableCol>
                            </TableRow>
                          ) : (
                            <strong>Nejsou data</strong>
                          )}
                        </tbody>
                      </Table>
                    </TableCol>
                  </TableRow>
                )}
              </>
            );
          })}
        </tbody>
      </Table>
    </Page>
  );
};

export default BuildingsPage;
