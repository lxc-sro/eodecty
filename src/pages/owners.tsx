import React, {  useState } from "react";
import Page from "../components/Page";
import Box from "../components/Box";
import { Table, TableRow, TableCol } from "../components/TableComponents";
import Chart from "react-apexcharts";
import { theme } from "../app/theme";
import { useOdectyQuery } from "../generated/graphql";
import Select from "react-select";
import styled from "../app/styled";
import cookies from "js-cookie";


const OwnersPage = () => {
  const [mericeSmlouvy, setMericeSmlouvy] = useState([{}]);
  const [isSetMerice, setIsSetMerice] = useState(false);
  const [selected, setselected] = useState(0);
  const [selectedDum, setSelectedDum] = useState(0);
  const [selectedJednotka, setSelectedJednotka] = useState(0);
  const [selectedOsoba, setSelectedOsoba] = useState("");

  const user = cookies.get("token");

  const { data, loading, error } = useOdectyQuery({
    variables: { token: user },
  });

  if (selected > 0 && isSetMerice === true) {
    data?.dataByToken?.Domy?.map((dum) =>
      dum.Jednotky?.map((jednotka) =>
        jednotka.Osoby?.map((osoba) =>
          osoba.sml?.map((smlouv) => {
            if (smlouv.ID === selected) {
              setMericeSmlouvy(smlouv.Merice);
              setIsSetMerice(false);
            }
          })
        )
      )
    );
  }

  // console.log(selected);
  // console.log("smlostate", mericeSmlouvy);

  if (loading || error || !data.dataByToken.Domy) {
    return <Page title="Vlastníci">Načítání</Page>;
  }

  return (
    <Page title="Vlastníci">
      <SearchRow>
        <SearchBox>
          <Select
            options={data.dataByToken?.Domy?.filter((dum) =>
              selectedDum > 0 ? dum.ID === selectedDum : dum.ID > 0
            )?.flatMap((dum) =>
              dum.Jednotky?.filter((jednotka) =>
                selectedJednotka > 0
                  ? jednotka.ID === selectedJednotka
                  : jednotka.ID > 0
              )?.flatMap((jednotka) =>
                jednotka.Osoby?.flatMap((osoba) => {
                  return { value: osoba.Token, label: osoba.Nazev };
                })
              )
            )}
            onChange={(val) => {
              if (!val) {
                setSelectedOsoba("");
              } else {
                setSelectedOsoba(val.value);
              }
            }}
            isDisabled={selectedJednotka === 0}
            isClearable
            placeholder="Vyberte vlastníka"
          />
        </SearchBox>
        <SearchBox>
          <Select
            options={data.dataByToken.Domy.filter((dum) =>
              selectedDum > 0 ? dum.ID === selectedDum : dum.ID !== null
            )?.flatMap((dum) =>
              dum.Jednotky?.flatMap((jednotka) => {
                return { value: jednotka.ID, label: jednotka.Oznaceni };
              })
            )}
            onChange={(val) => {
              if (!val) {
                setSelectedJednotka(0);
              } else {
                setSelectedJednotka(val.value);
              }
              setSelectedOsoba("");
            }}
            isDisabled={selectedDum === 0}
            isClearable
            placeholder="Vyberte byt"
          />
        </SearchBox>
        <SearchBox>
          <Select
            options={data.dataByToken.Domy.flatMap((dum) => {
              return { value: dum.ID, label: dum.Popis };
            })}
            onChange={(val) => {
              if (!val) {
                setSelectedDum(0);
              } else {
                setSelectedDum(val.value);
              }
              setSelectedJednotka(0);
              setSelectedOsoba("");
            }}
            isClearable
            placeholder="Vyberte dům"
          />
        </SearchBox>
      </SearchRow>

      <Table>
        <tbody>
          <TableRow>
            <TableCol>Jméno</TableCol>
            <TableCol>Byt</TableCol>
            <TableCol>Dům</TableCol>
          </TableRow>
          {(loading || error) && <TableRow>Načítám data</TableRow>}

          {data.dataByToken.Domy.filter((dum) =>
            selectedDum > 0 ? dum.ID === selectedDum : dum.ID > 0
          )?.map((dum) =>
            dum.Jednotky?.filter((jednotka) =>
              selectedJednotka > 0
                ? jednotka.ID === selectedJednotka
                : jednotka.ID > 0
            )?.map((jednotka) =>
              jednotka.Osoby?.filter((osoba) =>
                selectedOsoba !== ""
                  ? osoba.Token === selectedOsoba
                  : osoba !== null
              )?.map((osoba) =>
                osoba.sml?.map((sml) => (
                  <>
                    <TableRow
                      onClick={() => {
                        selected === sml.ID
                          ? setselected(0)
                          : setselected(sml.ID);
                        setIsSetMerice(true);
                      }}
                    >
                      <TableCol>{osoba.Nazev}</TableCol>
                      <TableCol>{jednotka.Oznaceni}</TableCol>
                      <TableCol>{dum.Oznaceni}</TableCol>
                    </TableRow>
                    {selected === sml.ID && (
                      <TableRow>
                        <TableCol colSpan={3}>
                          <Table>
                            <tbody>
                              <TableRow>
                                <TableCol>Přehled - {osoba.Nazev}</TableCol>
                              </TableRow>
                              {mericeSmlouvy !== [{}] &&
                                mericeSmlouvy?.filter(
                                  (meric: any) =>
                                    meric.NamereneHodnoty?.length > 0 &&
                                    meric.NamereneHodnoty !== null &&
                                    meric.NamereneHodnoty[0].KonecnyStav !==
                                      null
                                ).length === 0 && (
                                  <TableRow>Nejsou data</TableRow>
                                )}
                              {mericeSmlouvy !== [{}] &&
                                mericeSmlouvy
                                  ?.filter(
                                    (meric: any) =>
                                      meric.NamereneHodnoty?.length > 0 &&
                                      meric.NamereneHodnoty !== null &&
                                      meric.NamereneHodnoty[0].KonecnyStav !==
                                        null
                                  )
                                  ?.map((meric: any) => (
                                    <TableRow>
                                      <TableCol>
                                        <Chart
                                          type="line"
                                          series={[
                                            {
                                              name: meric.DruhMerice.Nazev,
                                              type: "line",
                                              id: "studena",
                                              color: theme.colors["blue"],
                                              data: meric.NamereneHodnoty.flatMap(
                                                (hodnota) => meric.DruhMerice.Jednotka === "kWh" ? hodnota.KonecnyStav/1000 : hodnota.KonecnyStav
                                              ),
                                            },
                                          ]}
                                          options={{
                                            xaxis: {
                                              categories: meric.NamereneHodnoty.flatMap(
                                                (hodnota) =>
                                                  new Date(
                                                    hodnota.DatumOdectu
                                                  ).toLocaleDateString()
                                              ),
                                            },

                                            tooltip: {
                                              shared: true,
                                            },
                                            dropShadow: {
                                              enabled: true,
                                              top: 0,
                                              left: 0,
                                              blur: 3,
                                              opacity: 0.5,
                                            },
                                            stroke: { curve: "smooth" },
                                            title: {
                                              text:
                                                meric.DruhMerice.Nazev +
                                                " - " +
                                                meric.VyrobniCislo,
                                            },
                                            markers: {
                                              size: 1,
                                            },
                                          }}
                                        />
                                      </TableCol>
                                    </TableRow>
                                  ))}
                            </tbody>
                          </Table>
                        </TableCol>
                      </TableRow>
                    )}
                  </>
                ))
              )
            )
          )}
        </tbody>
      </Table>
    </Page>
  );
};

const SearchRow = styled(Box)`
  flex-direction: row;
`;

const SearchBox = styled(Box)`
  width: 100%;
  padding: 0 ${({ theme }) => theme.spacing.default};
`;
export default OwnersPage;
